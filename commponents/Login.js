import React, { useState } from 'react';
import { View, Text, ScrollView, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import Register from './Login/Register';
import SingIn from './Login/SignIn';

export default function SingUp() {

    const [isActive, setIsActive] = useState(true)
    const handelPressButtonRegister = () => {
        setIsActive(false)
    }
    const handelPressButtonSingIn = () => {
        setIsActive(true)
    }
   
    
    return (
        <ScrollView>
            <View style={styles.buttonText}>
                <TouchableOpacity >
                    <Text style={[styles.text, isActive && styles.activeText]} onPress={handelPressButtonSingIn} >Вход</Text>
                </TouchableOpacity>
                <TouchableOpacity >
                    <Text style={[styles.text, !isActive && styles.activeText]} onPress={handelPressButtonRegister}>Регистрация</Text>
                </TouchableOpacity>
            </View>
            {isActive ? (<SingIn />
            ) : (
                <Register />)}

        </ScrollView>
    );
}

const styles = StyleSheet.create({
    activeText: {
        textShadowColor: 'rgba(0, 0, 0, 0.50)', // shadow color
        textShadowOffset: { width: -0.3, height: 1 }, // shadow offset
        textShadowRadius: 3, // shadow radius
    },
    text: {
        fontSize: 20,
        fontWeight: '600'
    },
    buttonText: {
        marginLeft: 70, marginTop: 200, flexDirection: 'row', gap: 50,
    },


});