import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default function CustomHeader() {
  return (
    <View style={styles.headerContainer}>
      <Text style={styles.headerTextOne}>Hello!</Text>
      <Text style={styles.headerTextTwo}>What's for dinner today?</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
  marginTop:-15,
    height: 90, // Увеличиваем высоту заголовка
    backgroundColor: '#ffff',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  headerTextOne: {
    marginTop:0,
    fontWeight: '600',
    fontSize: 30, // Размер шрифта заголовка
    color: 'black',
  },
  headerTextTwo: {
    marginTop:0,
    fontWeight: '400',
    fontSize: 20, // Размер шрифта заголовка
    color: 'black',
  },
});