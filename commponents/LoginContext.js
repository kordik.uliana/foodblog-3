import React, { createContext, useState } from 'react';

const LoginContext = createContext();

export const LoginProvider = ({ children }) => {
  const [isSignedIn, setSignedIn] = useState(false);//проверка на авторизацию
  const [name, setName] = useState('');//имя
  const [surname, setSurname] = useState('');//фамилия
  const [user, setUsers] = useState([])// информация о пользователе
  const [token, setToken] = useState(null); // Добавлено состояние для хранения токен

  const contextValue = {
    isSignedIn,
    setSignedIn,
    name,
    surname,
    setName,
    setSurname,
    user,
    setUsers,
    setToken,
    token,
  };

  return (
    <LoginContext.Provider value={contextValue}>
      {children}
    </LoginContext.Provider>
  );
};

export default LoginContext;