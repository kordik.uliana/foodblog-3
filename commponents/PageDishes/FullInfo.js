import React, { useState, useContext, useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Modal, Pressable, ToastAndroid } from 'react-native';
import { gStyle } from '../../styles/style';
import { AntDesign } from '@expo/vector-icons';
import CartContext from '../CartContext';
import SelectedContext from '../SelectedContext';




export default function FullInfo({ route, navigation }) { //Мы используем параметр route, чтобы получить данные о выбранной новости
  

    const [count, setCount] = useState(0);
    const { addToCart } = useContext(CartContext);
    const { addToSelectedDishes, selectedDishes, heartedDishes, updateHeartedStateForDish, removeDishesWithHeartState } = useContext(SelectedContext);
    const [modalFavoriteDish, setModalFavoriteDish] = useState(false);
    const [heartColor, setHeartColor] = useState(heartedDishes[route.params.name] || false);

    const GoFavoriteDish = () => {
        setModalFavoriteDish(!modalFavoriteDish)
        navigation.navigate('SelectedDish')
    }

    const handleClickHeart = () => {
        if (selectedDishes.length !== 0) {
            setModalFavoriteDish(false);
        } else {
            setModalFavoriteDish(true);
        }
        const upHeartState = !heartColor;
        setHeartColor(upHeartState);

        updateHeartedStateForDish(route.params.name, upHeartState);
        const selectedDishesArray = [];

        if (upHeartState) {
            const infoSelectedHeart = {
                name: route.params.name,
                cost: route.params.cost,
                img: route.params.img,
                category: route.params.category,
                gramm: route.params.gramm,
                full: route.params.full,
                calories: route.params.calories,
                carbohydrates: route.params.carbohydrates,
                protein: route.params.protein,
                fat: route.params.fat,
                review: route.params.review
            };
            selectedDishesArray.push(infoSelectedHeart);

            // Вызываем колбэк-функцию, передавая информацию о заказе
            addToSelectedDishes(selectedDishesArray);

        } else {
            removeDishesWithHeartState(route.params.name);
            ToastAndroid.showWithGravity(
                'Dish removed from favorites',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
            );
        }
    }
    //===========================================================================================
    const handleAddToOrder = () => {
        // Создаем объект с информацией о блюде
        const newDishes = [];

        const newDish = {
            name: route.params.name,
            cost: route.params.cost,
            img: route.params.img,
            category: route.params.category,
            gramm: route.params.gramm,
            full: route.params.full,
            calories: route.params.calories,
            carbohydrates: route.params.carbohydrates,
            protein: route.params.protein,
            fat: route.params.fat,
            count: route.params.count + count

        };
        newDishes.push(newDish);
console.log('newDishes',newDishes)


        // Добавляем новые блюда в корзину


        // Вызываем колбэк-функцию, передавая информацию о заказе
        addToCart(newDishes);
        navigation.goBack()
    };



    const increment = () => {
        setCount(count + 1);
    };

    const decrement = () => {
        if (count > 0) {
            setCount(count - 1);
        }
    }

    return (
        <>

            <Modal
                style={styles.modalWindow}
                animationType="none"
                transparent={true}
                visible={modalFavoriteDish}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                    setModalFavoriteDish(!modalFavoriteDish);
                }}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Want to see your list?</Text>

                        <View style={styles.modalButtonRow}>
                            <Pressable
                                style={[styles.buttonModal, styles.buttonCloseNo]}
                                onPress={() => setModalFavoriteDish(!modalFavoriteDish)}>
                                <Text style={styles.textStyleNo}>No</Text>
                            </Pressable>
                            <Pressable
                                style={[styles.buttonModal, styles.buttonCloseYes]}
                                onPress={GoFavoriteDish}>
                                <Text style={styles.textStyleYes}>Yes</Text>
                            </Pressable>
                        </View>

                    </View>
                </View>

            </Modal>



            <ScrollView >
                <TouchableOpacity style={styles.imageContainer} onPress={() => navigation.goBack()}>
                    <AntDesign name="left" size={24} color="#ffff" />
                </TouchableOpacity>

                <TouchableOpacity style={styles.imageContainerHeart} onPress={handleClickHeart}>
                    <AntDesign name="hearto" size={24} color={heartColor ? 'red' : 'black'} />
                </TouchableOpacity>

                <Image source={{ uri: route.params.img }}
                    style={styles.Image} />


                <View style={styles.containerText}>
                    <Text style={[gStyle.title, styles.header]}>{route.params.name} </Text>
                    <Text style={styles.cost}> ${route.params.cost} </Text>
                    <Text style={styles.gramm}>{route.params.gramm} </Text>
                    <Text style={styles.full}>{route.params.full} </Text>


                    <View style={styles.container}>
                        <View style={styles.row}>
                            <Text style={styles.value}>{route.params.calories}</Text>
                            <Text style={styles.label}>Kcal</Text>

                        </View>
                        <View style={styles.row}>
                            <Text style={styles.value}>{route.params.carbohydrates}</Text>
                            <Text style={styles.label}>Carbs</Text>

                        </View>
                        <View style={styles.row}>
                            <Text style={styles.value}>{route.params.protein}</Text>
                            <Text style={styles.label}>Protein</Text>

                        </View>
                        <View style={styles.row}>
                            <Text style={styles.value}>{route.params.fat}</Text>
                            <Text style={styles.label}>Fat</Text>

                        </View>

                    </View>
                </View>

            </ScrollView>

            <View style={styles.containerAddProduct}>
                <TouchableOpacity style={styles.addToOrderButton} onPress={handleAddToOrder}>
                    <Text style={styles.addToOrderText}>Add to order</Text>
                </TouchableOpacity>

                <View style={styles.containerCall}>
                    <TouchableOpacity onPress={decrement} style={styles.button}>
                        <AntDesign name="minus" size={15} color="black" />
                    </TouchableOpacity>
                    <Text style={styles.count}>{count}</Text>
                    <TouchableOpacity onPress={increment} style={styles.button}>
                        <AntDesign name="plus" size={15} color="black" />
                    </TouchableOpacity>
                </View>
            </View>
        </>
    );
}




const styles = StyleSheet.create({
    buttonModal: {
        borderRadius: 20,
        padding: 10,
        paddingHorizontal: 50,
        elevation: 2,
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,

    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,

    },
    textStyleNo: {
        color: 'black',

        fontWeight: 'bold',
        textAlign: 'center',
    },
    textStyleYes: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'center',
        fontSize: 15,
        fontWeight: 'bold',
    },
    modalButtonRow: {
        flexDirection: 'row',
        gap: 12
    },
    buttonCloseNo: {
        borderWidth: 1,
        backgroundColor: '#ffff'
    },
    buttonCloseYes: {
        backgroundColor: '#F04840',
    },
    imageContainerHeart: {
        position: 'absolute',
        top: 50,
        right: 15,
        zIndex: 1,
    },
    containerCall: {
        borderBlockColor: 'silver',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        gap: 10,
        flexDirection: 'row',
        borderRadius: 4,


    },
    containerAddProduct: {
        marginTop: 30,
        flexDirection: 'row-reverse',
        gap: 40,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,

    },

    button: {
        borderRightWidth: 1,
        borderLeftWidth: 1,
        borderColor: 'silver',
        padding: 8,

    },
    count: {
        fontSize: 20,
    },

    addToOrderButton: {
        backgroundColor: '#F04840',
        padding: 10,
        borderRadius: 5,
        alignItems: 'center',
        borderRadius: 20,
        width: 200,


    },
    addToOrderText: {
        color: '#1D1D1C',
        fontSize: 16,
        fontWeight: 'bold',
    },

    row: {
        flex: 1,
        backgroundColor: '#DCDCDC',
        borderRadius: 28,
        padding: 7,


    },
    label: {
        marginTop: -3,
        fontSize: 12,
        opacity: 0.5,
        textAlign: 'center', // Центрирование текста в ячейке

    },
    value: {
        fontWeight: 'bold',
        textAlign: 'center', // Центрирование текста в ячейке
        flex: 1, // Расширение ячейки для текста
    },
    container: {
        marginTop: 20,
        gap: 10,
        flexDirection: 'row'
    },
    Image: {
        width: '100%',
        height: 400,
        borderBottomLeftRadius: 50, // Задаем радиус границы для левого нижнего угла
        borderBottomRightRadius: 50, // Задаем радиус границы для правого нижнего угла
    },

    imageContainer: {
        position: 'absolute',
        top: 50,
        left: 15,
        zIndex: 1,
    },
    containerText: {
        padding: 20
    },
    full: {

        fontSize: 16,
        alignContent: 'center',
        marginTop: 20,

    },
    gramm: {
        opacity: 0.5
    },
    cost: {
        marginLeft: -7,
        fontSize: 20,
        color: '#A52A2A'
    },
    header: {
        fontWeight: '700',
        fontSize: 25,
        marginTop: 25
    }
});