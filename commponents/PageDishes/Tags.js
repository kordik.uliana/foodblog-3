import * as React from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';

export default function TagScreen({ route }) {
    const { tag, transformedData } = route.params;
    const navigation = useNavigation();

    const dishesByTag = transformedData.filter(dish => {
        return dish.tag?.includes(tag); // Проверяет, содержит ли массив определенный тег, возвращая true или false.
    });

    const uniqueDishesByTag = Array.from(new Set(dishesByTag.map(dish => dish.name))).map(name => {
        // Создаем массив с уникальными значениями
        return dishesByTag.find(dish => dish.name === name);
    });

    console.log('dishesByTag', dishesByTag, 'tag', tag);
    console.log('tag', tag);

    return (
        <View>
            <TouchableOpacity style={styles.backButton} onPress={() => navigation.goBack()}>
                <AntDesign name="left" size={24} color="black" />
            </TouchableOpacity>
            <View style={styles.tagHeader}>
                <Text style={styles.tagHeaderText}>#{tag}</Text>
            </View>
            <FlatList
                contentContainerStyle={styles.flatListContent}
                columnWrapperStyle={styles.columnWrapper}
                data={uniqueDishesByTag}
                numColumns={2}
                renderItem={({ item }) => (
                    <View style={styles.dishItem}>
                        <TouchableOpacity onPress={() => navigation.navigate('FullInfo', { ...item })}>
                            <Image source={{ uri: item.img }} style={styles.dishImage} />
                            <Text style={styles.dishName}>{item.name}</Text>
                            <Ionicons name='star' size={10} color='black' style={styles.starIcon}>
                                <Text>{item.review}</Text>
                            </Ionicons>
                        </TouchableOpacity>
                    </View>
                )}
                keyExtractor={item => item.key.toString()}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    backButton: {
        marginTop: 50,
        marginLeft: 20,
    },
    tagHeader: {
        marginTop: 40,
        marginLeft: 17,
        marginBottom: 10,
    },
    tagHeaderText: {
        fontSize: 25,
        color: '#F04840',
        opacity: 0.6,
    },
    flatListContent: {
        paddingBottom: 160,
        paddingTop: 10,
    },
    columnWrapper: {
        justifyContent: 'space-between',
    },
    dishItem: {
        marginTop: 10,
        marginBottom: 20,
        width: '50%',
        paddingHorizontal: 10,
    },
    dishImage: {
        width: '100%',
        height: 170,
        borderRadius: 20,
    },
    dishName: {
        marginTop: 5,
        width: 160,
        fontWeight: "500",
        color: 'black',
        fontSize: 15,
        marginBottom: 20,
    },
    starIcon: {
        marginTop: -20,
        opacity: 0.7,
    },
});