import React, { useState, useContext, useEffect } from "react";
import { View, FlatList, StyleSheet } from 'react-native';
import { gStyle } from '../styles/style';
import WelcomeText from './Header/WelcomeText';
import HomeCarousel from './Carousel/HomeCarousel';
import DishesList from './DishesList/List';
import Header from './Header/header';
import Footer from './Footer/Footer';
import CartContext from './CartContext';
import { DishProvider } from './DataDishContext'


export default function Main({ navigation }) {
    const [selectedCategory, setSelectedCategory] = useState('All'); // по умолчанию задаем выводим All \ отслеживание нужной категории 
    const [searchText, setSearchText] = useState('');
    const { isCartVisible,  cartItems, setIsCartVisible } = useContext(CartContext);

    useEffect(() => {
        if (cartItems.length === 0) {
            setIsCartVisible(false);
        }
    }, [cartItems, setIsCartVisible]);

   

    //    console.log('addToCart',addToCart,'cartItems',cartItems)

    const handleSearch = (text) => { // пользователь вводит текст
        setSearchText(text);//меняем состояние с содержимым
    }
    const components = [

        { type: 'WelcomeText', component: <WelcomeText /> },
        { type: 'HomeCarousel', component: <HomeCarousel onCategorySelected={setSelectedCategory} selectedCategory={selectedCategory} searchText={searchText} /> },
        { type: 'DishesList', component: <DishesList navigation={navigation} selectedCategory={selectedCategory} searchText={searchText} /> }, // данные категории присваиваем


    ];

    return (
        <DishProvider>
            <View style={gStyle.main}>
                <Header navigation={navigation} onSearch={handleSearch} onCategorySelected={setSelectedCategory} />
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={components}
                    renderItem={({ item }) => item.component} // проходимся по каждому элементу массива
                    keyExtractor={(item, index) => index.toString()} // устанавливаем ключи
                    style={[styles.container, isCartVisible && styles.containerInactive]}
                />
                {isCartVisible && (
                    <View style={styles.bottomContainer}>
                        <Footer navigation={navigation} />
                    </View>
                )}
            </View>
        </DishProvider>

    );
}
const styles = StyleSheet.create(
    {
        containerInactive: {
            marginBottom: 70
        },
        container: {
            borderBottomLeftRadius: 30, // закругление нижнего левого угла
            borderBottomRightRadius: 30,
            backgroundColor: '#ffff',
            overflow: 'hidden',
            zIndex: 3,
            padding: 20,

        },
        bottomContainer: {
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            height: 100,
            backgroundColor: 'black',
            zIndex: 2
        }
    }
)
