import React, { useContext,useEffect, useState } from 'react';
import { StyleSheet, ActivityIndicator, View } from 'react-native';
import FullInfo from './PageDishes/FullInfo'
import MyDrawer from './Menu/Drawer';
import Account from './Menu/Account';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ShoppingCart from './Menu/ShoppingCart';
import Pay from './Pay/Pay';
import Tags from './PageDishes/Tags'
import Login from './Login'
import List from './DishesList/List'
import LoginContext from './LoginContext';
import SelectedDish from './Menu/Account/SelectedDish'
import AsyncStorage from '@react-native-async-storage/async-storage';


const Stack = createNativeStackNavigator();


export default function Navigate() {
    const { isSignedIn, setSignedIn ,setToken} = useContext(LoginContext)
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        const checkAuth = async () => {
            try {
                const token = await AsyncStorage.getItem('authToken');
                if (token) {
                    setToken(token);//
                    setSignedIn(true);
                }
            } catch (error) {
                console.error('Failed to fetch authorization token:', error);
            }finally {
                setIsLoading(false);
            }
        };

        checkAuth();
    }, []);

    if (isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator />
            </View>
        );
    }

    return <>
        <Stack.Navigator>
            {isSignedIn ? (<>
                <Stack.Screen
                    name='Drawer'
                    component={MyDrawer}
                    options={{ headerShown: false, }}
                />
                <Stack.Screen
                    name='FullInfo'
                    component={FullInfo}
                    options={{ headerShown: false }} // Скрываем шапку 

                />
                <Stack.Screen
                    name='Account'
                    component={Account}
                    options={{ headerShown: false }} // Скрываем шапку 
                />
                <Stack.Screen
                    name='ShoppingCart'
                    component={ShoppingCart}
                    options={{ headerShown: false }} // Скрываем шапку 
                />
                <Stack.Screen
                    name='Pay'
                    component={Pay}
                    options={{ headerShown: false }} // Скрываем шапку 
                />

                <Stack.Screen
                    name='Tags'
                    component={Tags}
                    options={{ headerShown: false }} // Скрываем шапку 
                />
                 <Stack.Screen
                    name='List'
                    component={List}
                    options={{ headerShown: false }} // Скрываем шапку 
                />
                  <Stack.Screen
                    name='SelectedDish'
                    component={SelectedDish}
                    options={{ headerShown: false }} // Скрываем шапку 
                />
            </>) :
                (<>
                    <Stack.Screen
                        name='Login'
                        component={Login}
                        options={{ headerShown: false }} // Скрываем шапку 
                    />
                </>)}
        </Stack.Navigator>

    </>
} 

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });