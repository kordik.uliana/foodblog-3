import React, { createContext, useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const DishContext = createContext();

export const DishProvider = ({ children }) => {
  const [dishData, setDishData] = useState([]);
  const [categoriesData, setCategoriesData] = useState([]);

  useEffect(() => {
    const fetchDishData = async () => {
      try {
        const response = await fetch("http://192.168.0.104:1337/api/dishes?populate=*");
        const dishDataStrapi = await response.json();
        setDishData(dishDataStrapi.data);
        // console.log('dishDatas',dishData)
      } catch (error) {
        console.error('Error fetching dish data:', error);
       }
    };

    const fetchCategoriesData = async () => {
      try {
        const response = await fetch("http://192.168.0.104:1337/api/categories?populate=*");
        const CategoriesDataStrapi = await response.json();

        const jsonValue = JSON.stringify(CategoriesDataStrapi.data);
        await AsyncStorage.setItem('CategoriesDataStrapi',jsonValue);
        setCategoriesData(CategoriesDataStrapi.data);
      } catch (error) {
        console.error('Error fetching categories data:', error);
      }
    };

    // console.log('categoriesData',categoriesData.img)
    fetchDishData();
    fetchCategoriesData();
  }, []);

 

  // console.log('dishData:', dishData);

  const contextValue = {
    dishData,
    categoriesData,
    setCategoriesData
  };
  return (
    <DishContext.Provider value={contextValue}>
      {children}
    </DishContext.Provider>
  );
};

export default DishContext;