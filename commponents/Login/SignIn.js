import React, { useState, useContext, useEffect } from 'react';
import { View, Text, SafeAreaView, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import LoginContext from '../LoginContext';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import messaging from '@react-native-firebase/messaging';

export default function SingUp() {
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const { setSignedIn, setToken, } = useContext(LoginContext)
    const [error, setError] = useState('');
    

    const handleLogin = async () => {
        try {
            const response = await axios.post('http://192.168.0.104:1337/api/auth/local', {
                identifier: email,
                password: password
            });
            console.log('Well done!');
            console.log('User profile', response.data.user);
            console.log('User token', response.data.jwt);
    
            await AsyncStorage.setItem('authToken', response.data.jwt); // сохраняем в хранилище токен
            setToken(response.data.jwt);
            setSignedIn(true);

            const requestUserPermission = async () => {
                const authStatus = await messaging().requestPermission();
                const enabled =
                    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
                    authStatus === messaging.AuthorizationStatus.PROVISIONAL;
        
                if (enabled) {
                    console.log('Authorization status:', authStatus);
                }
            };
        
            // Получить токен устройства
            const getToken = async () => {
                const token = await messaging().getToken();
                await AsyncStorage.setItem('DeviceToken', token);
                if (token) {
                    const jwt = await AsyncStorage.getItem('authToken');
                    console.log('Device token:', token);
                    await axios.post('http://192.168.0.104:1337/api/auth/local/fcm', {
                        fcm:token
                    }, {
                        headers: {
                          Authorization: `Bearer ${jwt}`
                        }
                });
                    // Здесь можно сохранить токен в базу данных или использовать для отправки уведомлений
                }
            };
        
            requestUserPermission();
            getToken();

           
           
        } catch (error) {
            if (error.response) {
                // Обработка ответа об ошибке от сервера
                console.log('An error occurred:', error.response.data);
                const errorMessage = error.response.data?.error?.message || 'Invalid credentials';
                setError(errorMessage);
            } else if (error.request) {
                // Обработка ответа не получен
                console.log('No response received:', error.request);
                setError('No response from server');
            } else {
                // Обрабатываем другие ошибки
                console.log('Error:', error.message);
                setError('Something went wrong');
            }
        }
    
    };




    return (
        <SafeAreaView>
            <View style={styles.containerInput}>

                <View style={styles.inputContainer}>
                    <Text style={styles.label}>E-mail</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=""
                        value={email}
                        onChangeText={setEmail}
                        keyboardType=""
                    />
                </View>

                <View style={styles.inputContainer}>
                    <Text style={styles.label}>Password</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=""
                        value={password}
                        onChangeText={setPassword}
                        secureTextEntry
                    />
                </View>
                {error ? <Text style={styles.errorText}>{error}</Text> : null}
            </View>

            <TouchableOpacity style={styles.button} onPress={handleLogin}>
                <Text style={styles.buttonTextRegister} >Sing In</Text>
            </TouchableOpacity>

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    errorText: {
        color: 'red',
        marginLeft:100


    },
    button: {
        backgroundColor: '#F04840',
        paddingVertical: 10,
        marginHorizontal: 65,
        borderRadius: 10,
        marginTop: 20
    },

    buttonTextRegister: {
        fontSize: 17,
        color: '#ffff',
        fontWeight: '500',
        flex: 1,
        justifyContent: 'center',
        textAlign: 'center'
    },
    label: {
        opacity: 0.6,
        fontSize: 12,
        position: 'absolute',
        top: 5,
        left: 75,
        zIndex: 2
    },
    containerInput: {
        marginTop: 30,
        flexDirection: 'column',
        gap: 30
    },
    input: {
        marginHorizontal: 65,
        borderRadius: 10,
        paddingBottom: 5,
        paddingTop: 19,
        paddingHorizontal: 14,
        backgroundColor: '#E1ADA2'
    },

    text: {
        fontSize: 20,
        fontWeight: '600'
    },
    buttonText: {
        marginLeft: 70, marginTop: 200, flexDirection: 'row', gap: 50,
    },

    imageContainer: {
        marginTop: 50,
        marginLeft: 20
    },

});