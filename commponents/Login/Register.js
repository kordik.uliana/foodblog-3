import React, { useState, useContext } from 'react';
import { View, Text, ScrollView, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import LoginContext from '../LoginContext';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export default function SingIn() {
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const { setSignedIn, name, surname, setName, setSurname, setToken } = useContext(LoginContext)
    const [error, setError] = useState('');

    const username = name + ' ' + surname
    // console.log('username', username)
    const handleRegister = async () => {
        if (passwordConfirm !== password) {
            setError('Пароли не совпадают');
        } else {
            setError('Введите повторно свои данные')
        }
       

        try {
            const response = await axios.post('http://192.168.0.104:1337/api/auth/local/register', {
                username: `${name} ${surname}`,
                email: email,
                password: passwordConfirm,
            });
    
            console.log('Well done!');
            console.log('User profile', response.data.user);
            console.log('User token', response.data.jwt);
            await AsyncStorage.setItem('authToken', response.data.jwt);
            setToken(response.data.jwt);
            setSignedIn(true);
        } catch (error) {
            if (error.response) {
                // Сервер вернул ответ с ошибкой
                console.error('Error response:', error.response.data);
                setError(error.response.data.error.message || 'An error occurred');
            } else if (error.request) {
                // Запрос был сделан, но ответа не получено
                console.error('Error request:', error.request);
                setError('No response from server');
            } else {
                // Что-то случилось при настройке запроса
                console.error('Error message:', error.message);
                setError(error.message);
            }
        }
        
    };



    return (
        <ScrollView>
            <View style={styles.containerInput}>
                <View style={styles.inputContainer}>
                    <Text style={styles.label}>Name</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=""
                        value={name}
                        onChangeText={setName}
                    />
                </View>
                <View style={styles.inputContainer}>
                    <Text style={styles.label}>Surname</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=""
                        value={surname}
                        onChangeText={setSurname}
                    />
                </View>
                <View style={styles.inputContainer}>
                    <Text style={styles.label}>E-mail</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=""
                        value={email}
                        onChangeText={setEmail}
                        keyboardType=""
                    />
                </View>
                <View style={styles.inputContainer}>
                    <Text style={styles.label}>Password</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=""
                        value={password}
                        onChangeText={setPassword}
                        secureTextEntry
                    />
                </View>
                <View style={styles.inputContainer}>
                    <Text style={styles.label}>Confirm the password</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=""
                        value={passwordConfirm}
                        onChangeText={setPasswordConfirm}
                        secureTextEntry
                    />
                </View>
                <View style={styles.errorTextContainer} >{error ? <Text style={styles.errorText}>{error}</Text> : null}</View>
            </View>
            <TouchableOpacity style={styles.button} onPress={handleRegister}>
                <Text style={styles.buttonTextRegister}>Register</Text>
            </TouchableOpacity>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    errorTextContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    errorText: {
        color: 'red',
    },

    label: {
        opacity: 0.6,
        fontSize: 12,
        position: 'absolute',
        top: 5,
        left: 75,
        zIndex: 2
    },
    containerInput: {
        flex: 1,
        marginTop: 30,
        flexDirection: 'column',
        gap: 30
    },
    button: {
        backgroundColor: '#F04840',
        paddingVertical: 10,
        marginHorizontal: 65,
        borderRadius: 10,
        marginTop: 20
    },

    buttonTextRegister: {
        fontSize: 17,
        color: '#ffff',
        fontWeight: '500',
        flex: 1,
        justifyContent: 'center',
        textAlign: 'center'
    },
    input: {
        marginHorizontal: 65,
        borderRadius: 10,
        paddingBottom: 5,
        paddingTop: 19,
        paddingHorizontal: 14,
        backgroundColor: '#E1ADA2'
    },

    text: {
        fontSize: 20,
        fontWeight: '600'
    },
    buttonText: {
        marginLeft: 70, marginTop: 200, flexDirection: 'row', gap: 50,
    },

    imageContainer: {
        marginTop: 50,
        marginLeft: 20
    },

});