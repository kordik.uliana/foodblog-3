import React, { useState, useContext } from 'react';
import { View, StyleSheet, TextInput, TouchableOpacity, Image, Text } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import CartContext from '../CartContext';

export default function Footer({  navigation }) {

    const { cartItems, totalPrice } = useContext(CartContext);

     const totalArray = cartItems.flat(); //массив что исправляет вложенный массив 

    const uniqueArray = Array.from(new Set(totalArray.map(item => item.name))).map(name => {//создаем уникальный массив 
        return totalArray.find(item => item.name === name);
    });

    


    // console.log('cartItems', cartItems, 'uniqueArray', uniqueArray)
    // console.log('uniqueCout', uniqueCout,  'uniqueArray', uniqueArray)
    return (
        <View style={styles.container}>
            <Text style={styles.text} >Order</Text >
            <View style={styles.containerImage} >
                <FlatList
                    horizontal
                    data={uniqueArray}
                    renderItem={({ item }) => (
                        <TouchableOpacity onPress={() => navigation.navigate('ShoppingCart', {uniqueArray})} >
                            <Image source={{ uri: item.img }} style={styles.image} />
                        </TouchableOpacity>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
                <Text style={styles.totalCost}>${totalPrice}</Text>
            </View>
        </View>
    );
}
const styles = StyleSheet.create(
    {
        totalCost: {
            position: 'absolute',
            bottom: 2,
            left: 200,
            color: '#ffff',
            fontSize: 20,

        },

        containerImage: {
            marginTop: 2,
            width: 120
        },
        image: {
            borderRadius: 8,
            width: 30,
            height: 30,
            marginLeft: 10,
        },
        container: {
            position: 'absolute',
            marginBottom: 30,
            top: 55,
            left: 30,
            flexDirection: 'row',
            gap: 20,
        },
        text: {
            marginTop: 5,
            color: '#ffff',
            fontWeight: '600',
            fontSize: 20
        }
    }
)