import React, { useContext } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LoginContext from '../../LoginContext';

export default function InfoSite() {
  const navigation = useNavigation();
  const { setSignedIn } = useContext(LoginContext)

  const removeAuthToken = async () => {
    try {
      await AsyncStorage.removeItem('authToken');
      console.log('Authorization token removed successfully!');
      setSignedIn(false)
     
    } catch (error) {
      console.error('Failed to remove authorization token:', error);
    }
  };


  return (
    <SafeAreaView >
      <TouchableOpacity style={styles.containerData} onPress={() => navigation.navigate('SelectedDish')} >
        <AntDesign name="hearto" size={24} color={'red'} />
        <Text style={styles.textSelected} >selected</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.containerData} onPress={removeAuthToken} >
      <MaterialCommunityIcons name="exit-run" size={24} color="black" />
        <Text style={styles.textSelected} >Exit</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({

  containerData: {
    alignItems: 'center',
    marginLeft: 30,
    marginTop: 30,
    flexDirection: 'row',
    gap: 15
  },

  textSelected: {
    marginBottom: 2,
    fontSize: 20,
  },

});