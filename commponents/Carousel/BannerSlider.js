import React from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native';


const BannerSlider = ({ data, onSelectCategory, selectedCategory }) => {
    const handleCategorySelection = () => {
        onSelectCategory(data.category);

    };


    const buttonColor = selectedCategory === data.category ? '#F04840' : '#ffff';// selectedCategory === data.category выражение вернет true только в том случае, если selectedCategory и data.category имеют одинаковое значение и тип данных.
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={handleCategorySelection}  >
                <View style={[styles.BannerContainer, { backgroundColor: buttonColor }]}>
                <Image source={{ uri: data.image }} style={styles.BannerItem} /> 
                    <Text style={styles.name}>{data.category}</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
};

export default BannerSlider;

const styles = StyleSheet.create({
   
    BannerContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        height: 80,
        width: 80,
        borderColor: 'silver',
        borderWidth: 1,
        borderRadius: 20,
        padding: 10,
    },
    BannerItem: {
        height: 55,
        width: 55,
    },

    name: {
        fontWeight: "700",
        color: 'black',
        marginTop: 20,
        fontSize: 13,
    },
    selected: {
        backgroundColor: 'red',
    },

});