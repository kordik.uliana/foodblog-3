import messaging from '@react-native-firebase/messaging';

// Получение токена устройства для уведомлений
async function getToken() {
  const token = await messaging().getToken();
  console.log('Device Token:', token);
  return token;
}

// Отправка уведомления через FCM
async function sendPushNotification(token) {
  const message = {
    token: token,
    notification: {
      title: 'Вход в аккаунт',
      body: 'Вы успешно вошли в свой аккаунт.',
    },
  };

  try {
    const response = await messaging().send(message);
    console.log('Successfully sent message:', response);
  } catch (error) {
    console.log('Error sending message:', error);
  }
}

// Пример вызова функции отправки уведомления
const userToken = await getToken();
sendPushNotification(userToken);