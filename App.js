import 'react-native-gesture-handler';
import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, ActivityIndicator, View, Alert } from 'react-native';
import * as Font from 'expo-font';
import Navigate from './commponents/navigate';
import { NavigationContainer } from '@react-navigation/native';
import { CartProvider } from "./commponents/CartContext";
import { LoginProvider } from './commponents/LoginContext';
import { SelectedProvider } from './commponents/SelectedContext';
import { DishProvider } from './commponents/DataDishContext'
import messaging from '@react-native-firebase/messaging';
import { AppRegistry } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
// import firebaseAdmin from './commponents/Firebase.js/firebaseAdmin'



const Fonts = () => Font.loadAsync({
  'mono-bold': require('./assets/fonts/IBMPlexMono-Bold.ttf'),
  'mono-light': require('./assets/fonts/IBMPlexMono-Light.ttf'),
  'barlow': require('./assets/fonts/Barlow-ExtraLight.ttf')
});



export default function App() {
  // const [strapiDishData, setStrapiDishData] = useState([])
  const [font, setFont] = useState(false);
  //====================================================

  useEffect(() => {
    const loadResources = async () => {
      try {
        await Fonts();
        setFont(true);
      } catch (error) {
        console.error('Error loading fonts:', error);
      }
    };

    loadResources();
  }, []);

  useEffect(() => {
  //   const getToken = async () => {
  //     const token = await messaging().getToken();
  //     await AsyncStorage.setItem('DeviceToken', token);
  //     if (token) {
  //         const jwt = await AsyncStorage.getItem('authToken');
  //         console.log('Device token:', token);
  //         await axios.post('http://192.168.0.104:1337/api/auth/local/fcm', {
  //             fcm:token
  //         }, {
  //             headers: {
  //               Authorization: `Bearer ${jwt}`
  //             }
  //     });
  //         // Здесь можно сохранить токен в базу данных или использовать для отправки уведомлений
  //     }
  // };
  // getToken();
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      const { title, body } = remoteMessage.notification;
      Alert.alert(`${title}`, `${body}`);
    });

    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('Message handled in the background!', remoteMessage);
      const { title, body } = remoteMessage.notification;
      Alert.alert(`${title}`, `${body}`);
    });

    return unsubscribe;
    
  }, []);


  AppRegistry.registerComponent('app', () => App);

  //==================================================

  if (!font) {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
      </View>
    );
  }

  //=======================================================

  return (
    <DishProvider>
      <SelectedProvider>
        <LoginProvider>
          <CartProvider>
            <NavigationContainer>
              <Navigate />
              {/* <firebaseAdmin/>  */}
            </NavigationContainer>
          </CartProvider>
        </LoginProvider>
      </SelectedProvider>
    </DishProvider>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});