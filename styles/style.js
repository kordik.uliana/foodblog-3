import { StyleSheet, Text, View } from 'react-native';

export const gStyle = StyleSheet.create(
    {
        main: {
            flex: 1,
            backgroundColor: "#ffff",
         

        },
        title: {
            fontSize: 20,
            color: '#333',
            fontFamily: 'mono-light'

        },

        container:{
            flex: 1,
            flexDirection: 'row',
        }
    }
)